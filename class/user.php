<?
	class user
	{
		private $database;

		//Benoetigte Datenbank wird gemaped
		public function __construct($database)
		{
			$this->database = $database;

			// Session wird auf 20 min terminiert und dann gestartet
			session_cache_limiter(20);
			session_start();
		}

		//Login-Vorgang
		public function loginUser($value, $password)
		{	
			if(filter_var($value, FILTER_VALIDATE_EMAIL))
			{	
				$email = $value;
				//Passwort-Eingabe wird verschlüsselt
				$password = $this->encrypt($password);

				// Wenn der User geprueft wurde, werden Sessions erstellt
				$loginDataValid = $this->checkUser($email, $password);
				if($loginDataValid  == 'success')
				{
					// Alle Daten der User holen
					$db_res = $this->database->runSQLQuery("SELECT * FROM user WHERE email LIKE '".$email."' ");
					
					// Daten in ein Array geben
					$row = mysql_fetch_array($db_res, MYSQL_ASSOC);

					// Session ueber Loggedin-Status wird auf success gesetzt
					$_SESSION['isLoggedin'] = 'success';
					// Session mit dem Usernamen wird gesetzt
					$_SESSION['userID'] = $row['id'];

					return('success');
				}
				else
				{
					return('failure');
				}
			}
			else
			{
				$username = $value;
				//Passwort-Eingabe wird verschlüsselt
				$password = $this->encrypt($password);

				// Wenn der User geprueft wurde, werden Sessions erstellt
				$loginDataValid = $this->checkUser($username, $password);
				if($loginDataValid  == 'success')
				{
					// Alle Daten der User holen
					$db_res = $this->database->runSQLQuery("SELECT * FROM user WHERE username LIKE '".$username."' ");
					
					// Daten in ein Array geben
					$row = mysql_fetch_array($db_res, MYSQL_ASSOC);

					// Session ueber Loggedin-Status wird auf success gesetzt
					$_SESSION['isLoggedin'] = 'success';
					// Session mit dem Usernamen wird gesetzt
					$_SESSION['userID'] = $row['id'];

					return('success');
				}
				else
				{
					return('failure');
				}
			}
		}

		/* User aus Identitaet pruefen
		*	$email ist die email des Users
		*	$password ist das eingegebene Password bereits md5 verschlüsselt
		*/
		private function checkUser($value, $password)
		{	
			if(filter_var($value, FILTER_VALIDATE_EMAIL))
			{
				$email = $value;
				//Passwörter aus der Datenbank auslesen
				$db_res = $this->database->runSQLQuery("SELECT * FROM user WHERE email LIKE '".$email."' ");
				
				$row = mysql_fetch_array($db_res, MYSQL_ASSOC);

				if($row['email'] == $email)
				{
					if($row['password'] == $password)
					{
						return('success');
					}
					else
					{
						return('failure');
					}
				}
				else
				{
					return('failure');
				}
			}
			else
			{
				$username = $value;
				//Passwörter aus der Datenbank auslesen
				$db_res = $this->database->runSQLQuery("SELECT * FROM user WHERE username LIKE '".$username."' ");
				
				$row = mysql_fetch_array($db_res, MYSQL_ASSOC);

				if($row['username'] == $username)
				{
					if($row['password'] == $password)
					{
						return('success');
					}
					else
					{
						return('failure');
					}
				}
				else
				{
					return('failure');
				}
			}
		}

		/* Funktion um einen neuen User anzulegen
		*	Eingabewerte sind hier:
		*	$email = Ist die Emailadresse mit dem sich er User angemeldet hat.
		*	$password = Ist das bereits mit md5 verschlüsselte Passwort.
		*/
		public function addUser($email, $password, $username){

			if(filter_var($email, FILTER_VALIDATE_EMAIL))
			{	
				//Passwort-Eingabe wird verschlüsselt
				$password = $this->encrypt($password);

				// Prüfung ob email schon in der Datenbank vorhaben ist.
				$db_res = $this->database->runSQLQuery("SELECT * FROM user WHERE email LIKE '".$email."' ");

				$row = mysql_fetch_array($db_res, MYSQL_ASSOC);
				if($row['email'] == $email)
				{
					return('failure');
				}
				else
				{
					// Prüfung ob der username schon in der Datenbank vorhaben ist.
					$db_res = $this->database->runSQLQuery("SELECT * FROM user WHERE username LIKE '".$username."' ");

					$row = mysql_fetch_array($db_res, MYSQL_ASSOC);
					if($row['username'] == $username)
					{
						return('failure');
					}
					else
					{
						// Neuer User wird in die Datenbank eingetragen
						$db_res = $this->database->runSQLQuery("INSERT INTO user (email, password, username) VALUES ('".$email."','".$password."','".$username."') ");
						
					}

					
					return('success');
				}	

			}
			else
			{
				return('failure');
			}
		}

		// Funktion zum verschlüsseln von dem Wert $value mit dem erweitertem md5 Verfahren
		private function encrypt($value)
		{	
			// Mit Hilfe einer FOR-Schleife wird der Wert 10.000 mal verhärtet
			for($i=0; $i<10000; $i++)
			{
				//Hier wird ist die MD5-Funktion mit dem Passwort
				$value = md5($value . 'je329%(/&%(0ru9w3rc9&/(&%837480ri0284ru2');
			}

			return($value);
		}

		// Funktion um zu prüfen ob der User noch angemeldet ist.
		public function isLoggedin()
		{
			//Prüfen ob die SESSION mit dem Namen isLoggegdIn vorhanden ist.
			if(isset($_SESSION['isLoggedin']) )
			{
				//Prüfen ob die SESSION richtig gesetzt ist.
				if($_SESSION['isLoggedin'] == 'success')
				{
					return('success');
				}
				else
				{
					return('failure');
				}
			}
			else
			{
				return('failure');
			}
		}

		// Funktion zum Prüfen der Existenz der Email
		public function isEmailInDatabase($email)
		{
			$db_res = $this->database->runSQLQuery("SELECT email FROM user WHERE email LIKE '".$email."' ");

			$row = mysql_fetch_array($db_res);
			if($row['email'] == $email)
			{
				return('success');
			}
			else
			{
				return('failure');
			}
		}

		// Funktion zum Prüfen der Existenz der Email
		public function isUsernameInDatabase($username)
		{
			$db_res = $this->database->runSQLQuery("SELECT username FROM user WHERE username LIKE '".$username."' ");

			$row = mysql_fetch_array($db_res);
			if($row['username'] == $username)
			{
				return('success');
			}
			else
			{
				return('failure');
			}
		}


		/* Funktion um den angemeldeten User wieder abzumelden. 
		* Hierbei werden keine Parameter übergeben sondern gleich die ganze Session zertört.
		*/
		public function logoutUser()
		{
			if($_SESSION['isLoggedin'] == 'success')
			{
				// Sessions werden zurück gesetzt.
				unset($_SESSION['isLoggedin']);
				unset($_SESSION['userID']);

				// Sessions werde zerstört.
				session_destroy();	
				return('success');
			}
			else
			{
				return('failure');
			}
			
		}


	}
?>