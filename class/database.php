<?

	class database
	{
		//Eigenschaften der Datenbank
		private $host;
		private $username;
		private $password;
		private $database;
		private $db_link;

		//Konstruktor mit gleichzeitigem Connect
		public function __construct($host, $username, $password, $database)
		{
			$this->host = $host;
			$this->username = $username;
			$this->password = $password;
			$this->database = $database;

			$this->connect();

			
		}

		//Funktion zum Verbinden mit der Datenbank
		private function connect()
		{
			//Funktion zum Login am Host
			$this->db_link = mysql_connect($this->host, $this->username, $this->password);

			//Funktion zur Datenbankauswahl
			if($this->db_link)
			{
				//Datenbankauswahl
				mysql_select_db($this->database,$this->db_link) or die('<p>Die Verbindung zur Datenbank ist fehlgeschlagen!</p>');
			}
			else
			{
				//Fehlerausgabe
				die('<p>Die Verbindung zur Datenbank ist fehlgeschlagen!</p>');
			}
		}

		//Funktion zum ausfuehren von SQL Befehlen
		public function runSQLQuery($sql)
		{
			if($this->db_link)
			{
				$db_res = mysql_query($sql, $this->db_link) or die(mysql_error() .'<br />'. $sql);
				return($db_res);
			}
			else
			{
				//Fehlerausgabe
				die('<p>Die Verbindung zur Datenbank ist fehlgeschlagen!</p>');
			}
			
		}

		//Funktion zum Beenden der Verbindung der Datenbank
		public function close()
		{
			mysql_close($this->db_link);
		}

	}

?>